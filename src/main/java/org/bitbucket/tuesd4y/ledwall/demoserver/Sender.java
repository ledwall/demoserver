package org.bitbucket.tuesd4y.ledwall.demoserver;

import org.bitbucket.tuesd4y.ledwallapi.networking.MessageSender;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.SocketException;
import java.nio.charset.Charset;

/**
 * @author Christopher Stelzmüller
 * @version 1.0
 * created on 23.12.2016
 */

@RequestScoped
public class Sender extends MessageSender {

    public Sender() throws SocketException {
        this(CDI.current().select(Config.class).get());
    }

    @Inject
    public Sender(Config config) throws SocketException {
        super(config.getIp(), config.getPort());
    }


    protected void sendRawMessage(String s) {
        byte[] byteMessage = s.getBytes(Charset.forName("UTF-8"));
        try {
            socket.send(new DatagramPacket(byteMessage, byteMessage.length, getIpAddress(), getPort()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
