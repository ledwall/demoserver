package org.bitbucket.tuesd4y.ledwall.demoserver;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;

/**
 * @author Christopher Stelzmüller
 * @version 1.0
 *          created on 23.12.2016
 */

@WebServlet("/config")
public class ConfigServlet extends HttpServlet{
    private final Config config;

    @Inject
    public ConfigServlet(Config config) {
        this.config = config;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try(PrintWriter writer = resp.getWriter()) {
            String ip = req.getParameter("ip");
            String port = req.getParameter("port");
            if(ip != null || port != null) {
                if (ip != null) {
                    config.setIp(InetAddress.getByName("ip"));
                    writer.write(String.format("set ip to %s", ip));
                }
                if(port != null) {
                    config.setPort(Integer.parseInt(port));
                    writer.write(String.format("set port to %s", port));
                }
            }

            else {
                writer.write("no message...");
            }
        }
    }
}
