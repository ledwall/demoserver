package org.bitbucket.tuesd4y.ledwall.demoserver;

import org.bitbucket.tuesd4y.ledwallapi.entity.Colour;
import org.bitbucket.tuesd4y.ledwallapi.entity.LedWallMode;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Christopher Stelzmüller
 * @version 1.0
 * created on 23.12.2016
 */

@RequestScoped
@WebServlet("/demo")
public class MessageServlet extends HttpServlet{

    private final Sender sender;

    @Inject
    public MessageServlet(Sender sender) {
        this.sender = sender;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String msg = req.getParameter("msg");
        String colour = req.getParameter("colour");


        if(msg == null) {
            try(PrintWriter writer = resp.getWriter()) {
                writer.write("no message...");
            }
            return;
        }

        if(colour != null) {
            Colour c = new Colour(255, 255, 255);
            String[] colours = colour.split("x");
            c.setR(Integer.parseInt(colours[0]));
            c.setG(Integer.parseInt(colours[1]));
            c.setB(Integer.parseInt(colours[2]));

            sender.sendModeChange(LedWallMode.MESSAGE);
            sender.sendMessage(msg, c);
        } else {

            sender.sendModeChange(LedWallMode.MESSAGE);
            sender.sendMessage(msg);
        }

        try(PrintWriter writer = resp.getWriter()) {
            writer.write("accepted");
        }
    }
}
