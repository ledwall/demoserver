package org.bitbucket.tuesd4y.ledwall.demoserver;

import javax.ejb.Singleton;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author Christopher Stelzmüller
 * @version 1.0
 * created on 23.12.2016
 */

@Singleton
public class Config {
    private InetAddress ip = InetAddress.getByName("10.0.0.78");
    private int port = 8888;

    public Config() throws UnknownHostException {
    }

    public InetAddress getIp() {
        return ip;
    }

    public Config setIp(InetAddress ip) {
        this.ip = ip;
        return this;
    }

    public int getPort() {
        return port;
    }

    public Config setPort(int port) {
        this.port = port;
        return this;
    }
}
